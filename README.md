####For the creative part, I add the night mode in the setting which can switch the backgroud color to black and protect eyes

####I upload the apk file so you can directly run my project
  

##Generata Signed Apk by map.jks
 
##The first

![](https://i.imgur.com/XyFH0qF.png)

####1.Delete the code inside the red box

####2.Generata Signed Apk by map.jks 
#### map.jks in the root directory
keyAlias 'map'

keyPassword 'map123'

storeFile map.jks

storePassword 'map123'

##The second

 ![](https://i.imgur.com/Ell6CB4.png)

####Using the Api, my project need the map.jks file. To make it work, you need change the directory in app level gradle or the default path is 'C:/map.jks'

(10 points) User can login/logout, and login information is stored using Firebase
(20 points) On login, there is a map of the current area which displays pins corresponding to photos
(10 points) Logged in user�s posts are differentiated in a very clear way
(15 points) Clicking on a pin opens a new fragment/activity with the posters username, the date/location the photo was taken, and the photo itself
(15 points) User can click button to take a photo to post, which stores the photo and all relevant information
 Location should be from the GPS
(10 points) Map updates immediately with the users new post
(5 points) Creative portion(s) - build your own feature

Total: 85 / 100