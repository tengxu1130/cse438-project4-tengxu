package com.example.imagemap

import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.GeoPoint
import java.io.Serializable

class Image :Serializable{
    var address: String = ""
    var email: String = ""
    var imgUrl : String = ""
    var title :String =""
    var des :String =""
    var geoPoint: GeoPoint? = null

    constructor()

    constructor(address:String, email:String, imgUrl:String, title:String, des:String, geoPoint: GeoPoint?):this(){
        this.address = address
        this.email = email
        this.imgUrl = imgUrl
        this.title = title
        this.des = des
        this.geoPoint = geoPoint
    }

    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
                "email" to email,
                "geoPoint" to geoPoint,
                "address" to address,
                "imgUrl" to imgUrl,
                "title" to title,
                "des" to des
        )
    }

    override fun toString(): String {
        return "Image(address='$address', email='$email', imgUrl='$imgUrl', title='$title', des='$des', geoPoint=$geoPoint)"
    }
}