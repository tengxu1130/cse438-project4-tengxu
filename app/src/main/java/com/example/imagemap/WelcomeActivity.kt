package com.example.imagemap

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.Toast


class WelcomeActivity : AppCompatActivity() {
    private val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION)
    private val mPermissionList = ArrayList<String>()
    private val mRequestCode = 0x1
    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        checkP()
    }

    private fun checkP() {
        mPermissionList.clear()
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(this@WelcomeActivity, permission) != PackageManager.PERMISSION_GRANTED) {
                mPermissionList.add(permission)
            }
        }
        if (!mPermissionList.isEmpty()) {
            ActivityCompat.requestPermissions(this@WelcomeActivity, permissions, mRequestCode)
        } else {
            handler.sendEmptyMessageDelayed(0, 1000)
        }
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            0x1 -> for (i in 0 until grantResults.size) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this,"You have unprivileged permissions that may affect your use",Toast.LENGTH_SHORT).show()
                    }else{
                        handler.sendEmptyMessageDelayed(0, 1000)
                    }
            }

        }
    }
    private var  handler = object:Handler(){
        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
            val intent = Intent()
            intent.setClass(this@WelcomeActivity,LoginActivity::class.java)
            startActivity(intent)

            finish()
        }
    }
}