package com.example.imagemap

import android.os.Bundle
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.github.chrisbanes.photoview.PhotoView


class ImageDetailActivity :BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        onSetTitle("Image")
        var url =  intent.getStringExtra("url")
        val iv = findViewById<PhotoView>(R.id.photoView)
        Glide.with(this@ImageDetailActivity).load(url).into(iv)
    }
}


