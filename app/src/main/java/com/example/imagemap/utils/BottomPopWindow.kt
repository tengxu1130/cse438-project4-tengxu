package com.example.imagemap.utils

import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.bumptech.glide.Glide
import com.example.imagemap.Image
import com.example.imagemap.ImageDetailActivity
import com.example.imagemap.R

class BottomPopWindow private  constructor(): PopupWindow() {
    private lateinit var btn_cancel:View
    private lateinit var mainLayiout:View
    private lateinit var mMenuView: View
    private lateinit var  activity: Context
    private lateinit var  image: Image
    private val clickListener = View.OnClickListener {
        // 销毁弹出框
        dismiss()
    }


     constructor (context: Context, image: Image):this() {
        activity = context
        this.image = image
        initView()
    }

    private fun initView() {

        val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        mMenuView = inflater.inflate(R.layout.popwindow_layout, null, false)
        btn_cancel = mMenuView.findViewById(R.id.share_pop_btn_cancel)
        mainLayiout = mMenuView.findViewById(R.id.share_pop_layout)
        var ivImg:ImageView = mMenuView.findViewById(R.id.iv_img)
        var tvEmail:TextView = mMenuView.findViewById(R.id.tv_email)
        var tvAddress:TextView = mMenuView.findViewById(R.id.tv_address)
        var tvDes:TextView = mMenuView.findViewById(R.id.tv_des)
        tvAddress.text=image.address
        tvEmail.text=image.email
        tvDes.text=image.des
        Glide.with(activity).load(image.imgUrl).into(ivImg)
        // 取消按钮
        btn_cancel.setOnClickListener(clickListener)
        mainLayiout.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                var intent = Intent(activity,ImageDetailActivity::class.java)
                intent.putExtra("url",image.imgUrl)
                activity.startActivity(intent)
                dismiss()
            }

        })
        this.contentView = mMenuView
        this.width = ViewGroup.LayoutParams.MATCH_PARENT
        this.height = ViewGroup.LayoutParams.WRAP_CONTENT
        this.isFocusable = true
        this.animationStyle = R.style.PopupAnimation
        setBackgroundDrawable(BitmapDrawable())
        isOutsideTouchable = true
        width = ViewGroup.LayoutParams.MATCH_PARENT
        height = ViewGroup.LayoutParams.MATCH_PARENT
        val mLayoutManager = LinearLayoutManager(activity)
        mLayoutManager.orientation = LinearLayoutManager.HORIZONTAL



    }
}