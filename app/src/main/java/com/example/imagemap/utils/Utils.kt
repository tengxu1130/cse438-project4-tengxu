package com.example.imagemap.utils

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.WindowManager
import java.io.FileInputStream
import java.io.FileNotFoundException

class Utils {
    companion object {

        fun getLoacalBitmap(url: String): Bitmap? {
            try {
                val fis = FileInputStream(url)
                return BitmapFactory.decodeStream(fis)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
                return null
            }

        }

        fun darkenBackgroud(act: Activity, alpha: Float) {
            if (alpha < 0 || alpha > 1) return
            val lp = act.window.attributes
            lp.alpha = alpha!!
            if (alpha == 1.0f) {
                act.window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
            } else {
                act.window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
            }
            act.window.attributes = lp
        }
    }
}