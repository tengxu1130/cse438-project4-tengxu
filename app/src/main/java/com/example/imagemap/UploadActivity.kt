package com.example.imagemap

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.support.annotation.Nullable
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import com.example.imagemap.utils.Utils
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_upload.*
import java.io.File

import java.text.SimpleDateFormat
import java.util.*


class UploadActivity : BaseActivity() {
    private val progressDialog by lazy {
        ProgressDialog(this)
    }

    private fun showProgressDialog(msg:String) {
        progressDialog.setMessage(msg)
        progressDialog.isIndeterminate = true
        progressDialog.show()
    }

    private fun hideProgressDialog() {
        if (progressDialog.isShowing) {
            progressDialog.dismiss()
        }
    }

    private var imageUrl = ""
    private var geoPoint: GeoPoint? = null
    private val permissions = Manifest.permission.CAMERA
    private val mRequestCode = 0x1
    public override fun onStop() {
        super.onStop()
        hideProgressDialog()
    }
    companion object {
        const val AUTHORITY = "com.example.imagemap.fileProvider"
        var TAG:String="UploadActivity"
        const val REQUEST_CODE_CAPTURE_RAW = 2
         val rootFolderPath = Environment.getExternalStorageDirectory().absolutePath + File.separator + "img"
        var imgUri: Uri? = null
        var imageFile: File? = null

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload)
        onSetTitle("Upload")
        ivImg.setOnClickListener {
            if (ContextCompat.checkSelfPermission(this@UploadActivity, permissions) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this@UploadActivity,  arrayOf(permissions), mRequestCode)
            }else{
                gotoCaptureRaw()
            }

        }
        send.setOnClickListener {
            upload()

        }
        tvAddress.setOnClickListener {
            startActivityForResult(Intent(this@UploadActivity,MapsActivity::class.java),100)
        }

    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,       grantResults: IntArray) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == mRequestCode) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && permissions[0] == Manifest.permission.CAMERA) {
                gotoCaptureRaw()
            } else{
                Toast.makeText(this@UploadActivity,"permission denied",Toast.LENGTH_SHORT).show()
            }
        }
    }
    private  fun upload() {
        var title = etTitle.text.toString()
        var address = tvAddress.text.toString()
        var des = etDes.text.toString()
        val db = FirebaseFirestore.getInstance()
        if (TextUtils.isEmpty(title)){
            onToast("Please enter a title")
            return
        }
        if (TextUtils.isEmpty(address)){
            onToast("Please select an address")
            return
        }
        if (TextUtils.isEmpty(des)){
            onToast("Please describe")
            return
        }
        if (TextUtils.isEmpty(imageUrl)){
            onToast("Please upload an image")
            return
        }
        var user = FirebaseAuth.getInstance().currentUser
        showProgressDialog("loading")
        if (user != null) {
            user?.let {
                // Name, email address, and profile photo Url
                val email :String= user.email!!
                var image = Image(address,email,imageUrl,title ,des,geoPoint)
                db.collection("image")
                        .add(image.toMap())
                        .addOnSuccessListener {
                            onToast("success")
                            setResult(Activity.RESULT_OK)
                            hideProgressDialog()
                            finish()

                        }.addOnFailureListener{
                            onToast("Error")
                            hideProgressDialog()
                        }
            }

        } else {
          onToast("please login again")
        }


    }
    private fun gotoCaptureRaw() {
        imageFile = createImageFile()
        imageFile?.let {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {  //如果是7.0以上，使用FileProvider，否则会报错
                intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                imgUri = FileProvider.getUriForFile(this, AUTHORITY, it)
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri) //设置拍照后图片保存的位置
            } else {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(it)) //设置拍照后图片保存的位置
            }
            intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString()) //设置图片保存的格式
            intent.resolveActivity(packageManager)?.let {
                startActivityForResult(intent, REQUEST_CODE_CAPTURE_RAW) //调起系统相机
            }
        }
    }
    private fun createImageFile(): File? {
        return try {
            var rootFile = File(rootFolderPath + File.separator + "capture")
            if (!rootFile.exists())
                rootFile.mkdirs()
            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            val fileName = "IMG_$timeStamp.jpg"
            File(rootFile.absolutePath + File.separator + fileName)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            data?.let {
                val address = data.getStringExtra("address")
                address?.let {
                    tvAddress.text =address
                }
                var lat = data.getDoubleExtra("lat",0.00)
                var log = data.getDoubleExtra("log",0.00)
               geoPoint = GeoPoint(lat,log)
            }

        }
        if (resultCode == Activity.RESULT_OK) {

            when (requestCode) {
                REQUEST_CODE_CAPTURE_RAW -> { //拍照成功后，压缩图片，显示结果
                    imageFile?.let {
                        showProgressDialog("upload image")
                        Log.e("path",it.path)
                        var storage = FirebaseStorage.getInstance()
                        var storageRef = storage.reference
                        var file = Uri.fromFile(File(it.path))
                        val riversRef = storageRef.child("image/${file.lastPathSegment}")
                        var uploadTask = riversRef.putFile(file)

                        val urlTask = uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                            if (!task.isSuccessful) {
                                task.exception?.let {
                                    hideProgressDialog()
                                    onToast("net error")
                                    throw it
                                }
                            }
                            return@Continuation riversRef.downloadUrl
                        }).addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                val downloadUri = task.result
                                hideProgressDialog()
                                val img = Utils.getLoacalBitmap(it.path)
                                ivImg.setImageBitmap(img)
                                imageUrl = downloadUri.toString()
                            } else {
                              hideProgressDialog()
                                onToast("net error")
                            }
                        }
                    }
                }


            }
        } else {
            Log.d("tag", "error$resultCode")
        }
    }


}