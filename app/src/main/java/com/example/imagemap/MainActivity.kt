package com.example.imagemap

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Looper
import android.support.v7.app.AppCompatDelegate
import android.util.Log
import android.view.*
import android.widget.PopupWindow
import com.example.imagemap.utils.BottomPopWindow
import com.example.imagemap.utils.Utils
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.QuerySnapshot
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity :BaseActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {



    companion object {
        const val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000*600


        const val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        floatBtn.setOnClickListener {
            startActivityForResult(Intent(this@MainActivity,UploadActivity::class.java),200)
        }
        getData()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        var id = item.itemId
        if (id == R.id.option_normal_1) {
            var mode = getSharedPreferences("img",Context.MODE_PRIVATE).getBoolean("mode",false)
            var edit =  getSharedPreferences("img",Context.MODE_PRIVATE)
            if (mode) {
                edit.edit().putBoolean("mode",false).commit()
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            } else {
                edit.edit().putBoolean("mode",true).commit()
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            }
                var intent =  Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            return true
        }
        if (id == R.id.option_normal_2) {
            var intent =  Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==200&&resultCode== Activity.RESULT_OK){
            getData()
        }
    }
    private fun getData() {
        var db = FirebaseFirestore.getInstance()

        db.collection("image")
                .get()
                .addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
                    if (task.isSuccessful) {
                        list.clear()
                        for (document in task.result!!) {
                           var data =  document.toObject(Image::class.java)
                            Log.e("data",data.toString())
                            list.add(data)
                        }
                        addMarket()
                    } else {
                       onToast("failed")
                    }
                })
    }
    private var list = mutableListOf<Image>()
    private lateinit var mMap: GoogleMap

    private lateinit var mLocationRequest: LocationRequest

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, callback, Looper.myLooper())
        mMap.setOnMarkerClickListener(this)
    }
    private val callback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            super.onLocationResult(locationResult)
            val address = LatLng(locationResult!!.lastLocation.latitude, locationResult.lastLocation.longitude)
            mMap.clear()
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(address, 15f))
        }
    }
    private fun addMarket(){
        mMap.clear()
        for (image in list){
            image.geoPoint?.let {
                val address = LatLng(image.geoPoint!!.latitude, image.geoPoint!!.longitude)
                if (image.email==FirebaseAuth.getInstance().currentUser!!.email){
                    var per = mMap.addMarker(MarkerOptions().position(address).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                            .title(image.title))
                    per.tag= image
                }else{
                    var per = mMap.addMarker(MarkerOptions().position(address).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                            .title(image.title))
                    per.tag= image
                }


            }

        }

    }
    override fun onMarkerClick(marker: Marker): Boolean {
        val image = marker.tag as Image
        showPopWindow(image)
        return true
    }
    private var menuWindow: BottomPopWindow? =null
    private fun showPopWindow(image: Image) {
        menuWindow?.apply {
            if(menuWindow!!.isShowing){
                menuWindow!!.dismiss()
                Utils.darkenBackgroud(this@MainActivity, 1.0f)
                return
            }

        }
        menuWindow = BottomPopWindow(this@MainActivity,image)
        menuWindow!!.setOnDismissListener {
            Utils.darkenBackgroud(this@MainActivity, 1.0f)
        }
        Utils.darkenBackgroud(this@MainActivity, 0.7f)
        menuWindow!!.showAtLocation(this.findViewById(R.id.ll_rootview),
                Gravity.BOTTOM or Gravity.CENTER_HORIZONTAL, 0, 0)

    }

}