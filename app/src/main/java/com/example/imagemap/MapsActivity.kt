package com.example.imagemap

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.widget.Toast

import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PointOfInterest

import java.io.IOException
import java.util.Locale


class MapsActivity : BaseActivity(), OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener, GoogleMap.OnPoiClickListener {
    companion object {
        const val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
        const val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2
    }
    private var mMap: GoogleMap? = null
    private var mLocationRequest: LocationRequest? = null

    private val callback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            super.onLocationResult(locationResult)
            val sydney = LatLng(locationResult!!.lastLocation.latitude, locationResult.lastLocation.longitude)
            mMap!!.clear()
            mMap!!.addMarker(MarkerOptions().position(sydney)
                    .title("address"))
            mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15f))

            mMap!!.setOnMarkerClickListener { marker ->
                marker.position
                val geocoder = Geocoder(this@MapsActivity, Locale.getDefault())
                val addresses: List<Address>
                try {
                    addresses = geocoder.getFromLocation(marker.position.latitude, marker.position.longitude, 1)
                    val address = addresses[0].getAddressLine(0)
                    val intent = Intent()
                    intent.putExtra("address", address)
                    intent.putExtra("lat", marker.position.latitude)
                    intent.putExtra("log", marker.position.longitude)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                true
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        onSetTitle("map")
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            200//刚才的识别码
            -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {//用户同意权限,执行我们的操作
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    mMap!!.isMyLocationEnabled = false
                    return
                }

            } else {//用户拒绝之后,当然我们也可以弹出一个窗口,直接跳转到系统设置页面
                Toast.makeText(this, "Permission was denied", Toast.LENGTH_SHORT).show()
            }
            else -> {
            }
        }


    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        if (ContextCompat.checkSelfPermission(this@MapsActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {//未开启定位权限
            //开启定位权限,200是标识码
            ActivityCompat.requestPermissions(this@MapsActivity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 200)
        } else {
            mMap!!.isMyLocationEnabled = false

            mLocationRequest = LocationRequest()

            mLocationRequest!!.interval = UPDATE_INTERVAL_IN_MILLISECONDS
            mLocationRequest!!.fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS

            mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, callback, Looper.myLooper())
        }
        mMap!!.setOnPoiClickListener(this)
        mMap!!.setOnMyLocationButtonClickListener(this)
        mMap!!.setOnMyLocationClickListener(this)
    }

    override fun onMyLocationClick(location: Location) {
        val sydney = LatLng(location.latitude, location.longitude)
        mMap!!.addMarker(MarkerOptions().position(sydney)
                .title("address"))
        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(sydney))
        Toast.makeText(this, "Current location:\n$location", Toast.LENGTH_LONG).show()
    }

    override fun onMyLocationButtonClick(): Boolean {
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show()

        return false
    }


    override fun onPoiClick(poi: PointOfInterest) {

        val intent = Intent()
        intent.putExtra("address", poi.name)
        intent.putExtra("lat",poi.latLng.latitude)
        intent.putExtra("log",poi.latLng.longitude)
        setResult(Activity.RESULT_OK, intent)
        finish()

    }


}
