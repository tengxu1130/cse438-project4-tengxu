package com.example.imagemap

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import android.support.v7.app.AppCompatDelegate



class App : Application() {

    override fun onCreate() {
        super.onCreate()
        var mode = getSharedPreferences("img", Context.MODE_PRIVATE).getBoolean("mode",false)
        if (mode) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
        MultiDex.install(this)
    }
}