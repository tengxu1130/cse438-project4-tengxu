package com.example.imagemap

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.TextView
import android.widget.Toast

open class BaseActivity :AppCompatActivity() {

    fun onSetTitle(title: String) {
        val toolbar = findViewById<Toolbar>(R.id.at_toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationIcon(R.drawable.back_arr)
        toolbar.setNavigationOnClickListener { finish() }
        val mTitle = toolbar.findViewById(R.id.at_title) as TextView
        mTitle.text = title
    }

    fun onToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
}